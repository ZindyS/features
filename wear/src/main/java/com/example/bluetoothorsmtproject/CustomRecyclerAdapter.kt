package com.example.bluetoothorsmtproject

import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.poster_item.view.*
import java.lang.Integer.min

class CustomRecyclerAdapter(val list : MutableList<Bitmap>) : RecyclerView.Adapter<CustomRecyclerAdapter.VH>() {
    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.imageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.poster_item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.img.setImageBitmap(list[position])
//        Glide.with(holder.img)
//            .load(Uri.parse(list[position]))
//            .into(holder.img)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}