package com.example.bluetoothorsmtproject

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.example.bluetoothorsmtproject.databinding.ActivityMainBinding
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream

class MainActivity : Activity(), DataClient.OnDataChangedListener {
    private val storage = "/somethinglol"
    private val TAG = "Errror"
    private lateinit var binding: ActivityMainBinding
    var cur = -1

    fun loadBitmapFromAsset(asset: Asset): Bitmap? {
        // convert asset into a file descriptor and block until it's ready
        val assetInputStream: InputStream? =
            Tasks.await(Wearable.getDataClient(this).getFdForAsset(asset))
                ?.inputStream

        return assetInputStream?.let { inputStream ->
            // decode the stream into a bitmap
            BitmapFactory.decodeStream(inputStream)
        } ?: run {
            Log.w(TAG, "Requested an unknown Asset.")
            null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Wearable.getDataClient(this@MainActivity).addListener(this)
    }

    override fun onDataChanged(p0: DataEventBuffer) {
        p0
            .filter { it.type == DataEvent.TYPE_CHANGED && it.dataItem.uri.path == storage }
            .forEach {
                val dataMap = DataMapItem.fromDataItem(it.dataItem).dataMap
                val list = dataMap.getDataMapArrayList("Spisok")
                val newcur = dataMap.getInt("cur")
                val ans = mutableListOf<Bitmap>()
                Thread {
                    for (i in list!!) {
                        ans.add(loadBitmapFromAsset(i.getAsset("im")!!)!!)
                    }
                    Log.d(TAG, "$newcur $cur")
                    if (newcur != cur) {
                        cur = newcur
                        runOnUiThread {
                            recyclerView.adapter = CustomRecyclerAdapter(ans)
                        }
                    }
                }.start()
            }
    }
}