package com.example.bluetoothorsmtproject

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.poster_item.view.*
import java.lang.Integer.min

class CustomRecyclerAdapter(val list : List<String>, val feature : ArrayList<Boolean>) : RecyclerView.Adapter<CustomRecyclerAdapter.VH>() {

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val btn = itemView.imageView2
        val img = itemView.imageView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.poster_item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        Glide.with(holder.img)
            .load(Uri.parse(list[position]))
            .into(holder.img)
        if (!feature[position]) {
            holder.btn.setColorFilter(Color.BLACK)
        } else {
            holder.btn.setColorFilter(Color.RED)
        }
        holder.btn.setOnClickListener {
            if (feature[position]) {
                feature[position]=true
                holder.btn.setColorFilter(Color.BLACK)
            } else {
                feature[position]=false
                holder.btn.setColorFilter(Color.RED)
            }
            MainActivity.onSomeClicked(position)
        }
    }

    override fun getItemCount(): Int {
        return min(list.size, feature.size)
    }
}