package com.example.bluetoothorsmtproject

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract.Data
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.gms.tasks.Task
import com.google.android.gms.wearable.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.ByteArrayOutputStream

class MainActivity : AppCompatActivity() {
    lateinit var sh : SharedPreferences

    companion object {
        private lateinit var client : DataClient
        private val storage = "/somethinglol"
        var savelist = arrayListOf<Asset>()
        var cur = 0
        lateinit var ed : SharedPreferences.Editor
        val list = listOf(
            "https://sport-dog.ru/wp-content/uploads/0/7/a/07a42b3cd7f6d4317d8e1274b3f56803.jpeg",
            "https://cdn.fishki.net/upload/post/2020/07/13/3367909/9caf06eba598301086fe2ed34ab4af54.jpg",
            "https://adonius.club/uploads/posts/2022-05/1653614049_65-adonius-club-p-sibirskii-lesnoi-kot-krasivo-foto-72.jpg",
            "https://chudo-prirody.com/uploads/posts/2021-08/1628909735_18-p-yevropeiskii-lesnoi-kot-foto-20.jpg",
            "https://adonius.club/uploads/posts/2022-05/1653594500_13-adonius-club-p-yevropeiskii-lesnoi-kot-krasivo-foto-15.jpg")
        var feature = arrayListOf(false, false, false, false, false)
        lateinit var adapter : CustomRecyclerAdapter
        lateinit var context : Context

        fun onSomeClicked(pos: Int) {
            feature[pos]=!feature[pos]
            adapter.notifyDataSetChanged()
            var smt = ""
            for (i in feature) {
                if (i) {
                    smt+='1'
                } else {
                    smt+='0'
                }
            }
            ed.putString("feature", smt)
            ed.commit()

            Glide.with(context)
                .asBitmap()
                .load(list[pos])
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        Log.d("Errror", "hui")
                        return true
                    }

                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: Target<Bitmap>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (feature[pos]) {
                            savelist.add(createAssetFromBitmap(resource!!))
                        } else {
                            savelist.remove(createAssetFromBitmap(resource!!))
                        }
                        createRequest()
                        return true
                    }
                })
                .preload()
        }

        private fun createRequest() {
            val request = PutDataMapRequest.create(storage)
            val dataMap = request.dataMap
            val ans = arrayListOf<DataMap>()
            for (i in savelist) {
                val dataMap1 = DataMap()
                dataMap1.putAsset("im", i)
                ans.add(dataMap1)
            }
            dataMap.putDataMapArrayList("Spisok", ans)
            dataMap.putInt("cur", ++cur)
            Log.d("Errror", "SMT123123")
            val putTask: Task<DataItem> = client.putDataItem(request.asPutDataRequest())
            putTask.addOnCompleteListener {
                Log.d("Errror", "saveWearableData complete listener")
            }
        }

        private fun createAssetFromBitmap(bitmap: Bitmap): Asset =
            ByteArrayOutputStream().let { byteStream ->
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream)
                Asset.createFromBytes(byteStream.toByteArray())
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        client = Wearable.getDataClient(this)
        sh = getSharedPreferences("myProject", 0)
        ed = sh.edit()
        val ftr = sh.getString("feature", "00000")!!
        for (i in ftr.indices) {
            if (ftr[i]=='1') {
                feature[i]=true
            }
        }
        cur = sh.getInt("cur", 0)
        adapter = CustomRecyclerAdapter(list, feature)
        recyclerView.adapter = adapter
        context = this
    }
}